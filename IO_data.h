/* Entrada y salida de datos */ 

#pragma once

#include <complex.h>
#include "Parameters.h"


int leer_binario(char const *archivo_entrada, COMPLEX *data);
int generar_archivo_binario(COMPLEX *data, char const *archivo_salida);

int leer_matriz_compleja(COMPLEX *data, char const *datos_real, char const *datos_imag);

int imprimir_matriz_compleja_a_archivo(COMPLEX *data, int filas, int cols, char const *nombre);
int imprimir_matriz_compleja_a_archivo_log(COMPLEX *data, int filas, int cols, char const *nombre);

int imprimir_matriz_float_a_archivo(FLOAT *data, int filas, int cols, char const *nombre);
    

int imprimir_vector_float(FLOAT *vector, int dim, char *nombre);

int imprimir_fila_de_matriz_complex(COMPLEX *data, int fila, int  Nfilas, int Ncols, int desde, int hasta);


/* Application constant definitions */

#pragma once

#include <stdio.h>
#include <complex.h>
#include <cuComplex.h>

/* cambiar a double o float as you want */ 
/* CAMBIAR DE FORM ACONSITENTE, NO ME DEJA NVCC PONER DIRECTAMENTE FOAT complex COMPLEX ...*/
typedef float FLOAT;
typedef float complex COMPLEX;


//typedef float CUFLOAT;
typedef  cuFloatComplex CUCOMPLEX;


/* DIMENSIONES DE LOS DATOS */
#ifndef M
#define M 2242  // m
#endif

#ifndef N
#define N 13716  // n  = pulsos
#endif


#ifndef NTHREADS_X
#define NTHREADS_X 16 
#endif

#ifndef NTHREADS_Y
#define NTHREADS_Y 16
#endif


/* Codigo de optimiacion */
#ifndef CODIGO_OPTIMIZACION
#define CODIGO_OPTIMIZACION 0 // 0 sin iptimizacion, 1 modificando patron de acceso a memoria global datos de entrada,  2 usando memoria compartida
#endif 

/* Valor para usar memoria paginable o no paginable en host */
#ifndef PINNED_MEMORY
#define PINNED_MEMORY 1 // 0 usa memoria paginable en host, 1 usa memoria no paginable acelerando la transferencia
#endif 


/* PARA ARRANCAR RAPIDO, NO PROCESO HEADER.MAT SINO QUE SACO YA LAS VARIABLES */

#ifndef PRIMER_PULSO_BARRIDO
#define PRIMER_PULSO_BARRIDO 36 // en C comienzan los arreglos en 0 -> 36 y no 37 como en matlab
#endif

#ifndef PULSOS_GRUPO
#define PULSOS_GRUPO 35 // 35 pulsos al mismo lugar = azimuth = elevation
#endif

#ifndef FILAS_SALIDA
#define FILAS_SALIDA 360
#endif


/* ARCHIVOS DE ENTRADA*/
#ifndef DATOS_ENTRADA_DATA_1_REAL
#define DATOS_ENTRADA_DATA_1_REAL "inputs/data_1_real.dat"
#endif


#ifndef DATOS_ENTRADA_DATA_1_IMAG
#define DATOS_ENTRADA_DATA_1_IMAG "inputs/data_1_imag.dat"
#endif


#ifndef DATOS_ENTRADA_DATA_2_REAL
#define DATOS_ENTRADA_DATA_2_REAL "inputs/data_2_real.dat"
#endif


#ifndef DATOS_ENTRADA_DATA_2_IMAG
#define DATOS_ENTRADA_DATA_2_IMAG "inputs/data_2_imag.dat"
#endif

/* BINARIOS A GENERAR */
#ifndef BINARIO_DATA_1
#define BINARIO_DATA_1 "inputs/data_1.bin"
#endif

#ifndef BINARIO_DATA_2
#define BINARIO_DATA_2 "inputs/data_2.bin"
#endif

/* ARCHIVOS DE SALIDA */
#ifndef SALIDA_POTENCIA_H
#define SALIDA_POTENCIA_H "prodzh.out"
#endif

#ifndef SALIDA_POTENCIA_V
#define SALIDA_POTENCIA_V "prodzv.out"
#endif

#ifndef SALIDA_FRECUENCIA_H
#define SALIDA_FRECUENCIA_H "prodih.out"
#endif

#ifndef SALIDA_FRECUENCIA_V
#define SALIDA_FRECUENCIA_V "prodiv.out"
#endif

#ifndef SALIDA_AUTOCORRELACION_A
#define SALIDA_AUTOCORRELACION_A "r_a.out"
#endif


#ifndef SALIDA_AUTOCORRELACION_B
#define SALIDA_AUTOCORRELACION_B "r_b.out"
#endif


#ifndef SALIDA_AUTOCORRELACION_VH
#define SALIDA_AUTOCORRELACION_VH "r_vh.out"
#endif

#ifndef SALIDA_COEF_CORRELACION
#define SALIDA_COEF_CORRELACION "prodRho.out"
#endif

#ifndef SALIDA_CORRIMIENTO_DE_FASE
#define SALIDA_CORRIMIENTO_DE_FASE "prod_Phi.out"
#endif

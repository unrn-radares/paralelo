#include <stdio.h>
#include <stdlib.h>
#include <iostream>
//#include <cblas.h>

#include "Parameters.h"
#include "IO_data.h"
#include "Procesador_meteorologico.h"

//#include "Operaciones_matriz.h"




using namespace std;

// data_1 es 1 matriz en la tercer dimension de los datos originales
// data_2 es 2 matriz en la tercer dimension de los datos originales


int
main(int argc, char **argv) {


	int matrix_size = sizeof(COMPLEX) * M * N;
	/* declaracion de matrices a procesar*/
	COMPLEX *data_H;
	cudaHostAlloc((void**) &data_H, matrix_size, cudaHostAllocDefault);
	COMPLEX *data_V;
	cudaHostAlloc((void**) &data_V, matrix_size, cudaHostAllocDefault);

	if(!data_H || !data_V) {
		cout << "MAIN no aloca matrices " << endl;
		exit(-1);
	}

		/* datos de la placa */
	int card;
	cudaGetDevice(&card);
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, card);
	cout << "\nDevice Selected " << card << " " << deviceProp.name << " " <<deviceProp.canMapHostMemory << " " << endl << endl;

	// Lectura de archivos
	if (leer_binario(BINARIO_DATA_1, data_V) == EXIT_FAILURE) {
		leer_matriz_compleja(data_V, DATOS_ENTRADA_DATA_1_REAL, DATOS_ENTRADA_DATA_1_IMAG);
		generar_archivo_binario(data_V, BINARIO_DATA_1);
	}

	if (leer_binario(BINARIO_DATA_2, data_H) == EXIT_FAILURE) {
		leer_matriz_compleja(data_H, DATOS_ENTRADA_DATA_2_REAL, DATOS_ENTRADA_DATA_2_IMAG);
		generar_archivo_binario(data_H, BINARIO_DATA_2);
	}

	cout << "Proceso datos" << endl;
	procesar_datos(data_V, data_H);

	cout << "Todo ok!" << endl;

	cudaFreeHost(data_H);
	cudaFreeHost(data_V);
	return(0);
}

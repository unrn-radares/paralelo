#!/bin/bash

sudo apt-get update

#######################
## cBLAS library
#######################

# Install required packages
sudo apt-get install gcc gfortran libblas-dev

# Go to /tmp
pushd /tmp

echo "Downloading CBLAS library..."
wget http://www.netlib.org/blas/blast-forum/cblas.tgz -O cblas.tgz

echo "Extracting CBLAS library..."
tar -xzvf cblas.tgz

cd CBLAS
rm Makefile.in
ln -s Makefile.LINUX Makefile.in

# Replace libblas.a path

# Return to previous directory
popd

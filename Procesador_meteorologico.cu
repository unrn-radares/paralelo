/*
 	Procesador Meteorologico

 	Monica Denham 

 	febrero 2016

*/

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <cuda_runtime.h>


#include "Procesador_meteorologico.h"
#include "IO_data.h"
//#include "Operaciones_matriz.h"
#include "gpu_timer.h"
#include "cpu_timer.h"


#include <cuComplex.h>

#define CUDART_PI_F 3.141592654f
#define T 1



using namespace std;

#define angle(x) (atan2(cimag(x),creal(x)))

/*   HEADERS   */
int procesar_datos(COMPLEX *data_V, COMPLEX *data_H);


int calcular_potencia(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz);
int calcular_frecuencia(CUCOMPLEX *d_data, FLOAT *d_prodi);
int calcular_autocorrelacion(CUCOMPLEX *d_data1, int desdeCol1, int hastaCol1, CUCOMPLEX *d_data2, int desdeCol2, int hastaCol2, CUCOMPLEX *R);
int funcion_coef_correlacion(CUCOMPLEX *d_R_vh, CUCOMPLEX *d_prodzh, CUCOMPLEX *d_prodzv, FLOAT *d_prodRho);
int calcular_corrimiento_fase(CUCOMPLEX *d_R_vh, CUCOMPLEX *d_R_phi);


int calcular_potencia_optimizacion_1(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz);
int calcular_frecuencia_optimizacion_1(CUCOMPLEX *d_data, FLOAT *d_prodi);


int calcular_potencia_optimizacion_2(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz);
int calcular_frecuencia_optimizacion_2(CUCOMPLEX *d_data, FLOAT *d_prodi);



/* kernels utilizados */
__global__ void kernel_calcular_potencia(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz); // deprecated
__global__ void kernel_nueva_potencia(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz);
__global__ void kernel_calcular_frecuencia(CUCOMPLEX *d_data, FLOAT *d_prodi); 
__global__ void	kernel_calcular_autocorrelacion(CUCOMPLEX *d_data1, int desdeCol1, int hastaCol1,CUCOMPLEX *d_data2, int desdeCol2, int hastaCol2, CUCOMPLEX *d_R);
__global__ void	kernel_calcular_coef_correlacion(CUCOMPLEX *d_R_vh, CUCOMPLEX *d_prodzh, CUCOMPLEX *d_prodzv, FLOAT *d_prodRho);
__global__ void	kernel_calcular_corrimiento_fase(CUCOMPLEX *d_R_vh, CUCOMPLEX *d_R_phi);

/* kernels optimiacion */
__global__ void kernel_calcular_potencia_optimizacion_1(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz);
__global__ void kernel_calcular_potencia_optimizacion_2(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz);



//__device__ COMPLEX producto_complejos(COMPLEX a, COMPLEX b);
__device__ CUCOMPLEX producto_cuFloatComplex(CUCOMPLEX a, CUCOMPLEX c);
__device__ CUCOMPLEX conjugado_cuFloatComplex(CUCOMPLEX a);
__device__ CUCOMPLEX suma_cuFloatComplex(CUCOMPLEX a, CUCOMPLEX b);
__device__ CUCOMPLEX division_cuComplex(CUCOMPLEX a, FLOAT b) ;
__device__ FLOAT kernel_angle(CUCOMPLEX valor);
__device__ FLOAT abs_cuComplex(CUCOMPLEX num);


/* Agrego punteros a funciones para llamar a las funciones segun codigo de optimizacion */
typedef int (*p_funcion_potencia_type)(CUCOMPLEX *,CUCOMPLEX *);
typedef int (*p_funcion_frecuencia_type)(CUCOMPLEX *,FLOAT *);
typedef int (*p_funcion_autocorrelacion_type)(CUCOMPLEX *, int, int, CUCOMPLEX *, int, int, CUCOMPLEX *);


/*  BODIES */

/*	NO ENTRAN EN MEMORIA DE LA GPU TODOS LOS DATOS. ENTONCES CALCULO H, V POR SEPARADO */
int procesar_datos(COMPLEX *data_V, COMPLEX *data_H){
	cout << "Inicio proceso de datos" << endl; 

//	int size_auxiliares = sizeof(FLOAT) * 360 * M;
	int resultados_size_complex = sizeof(COMPLEX) * 360 * M;
	int resultados_size_cuComplex = sizeof(CUCOMPLEX) * 360 * M;
	int resultados_size_float = sizeof(FLOAT) * 360 * M;

	int matrix_size = sizeof(COMPLEX) * N * M;
	int matrix_size_cuComplex = sizeof(CUCOMPLEX) * N * M;

	// aca aloco para cada resultado, en cpu tengo memoria para potencia (H y V) y frecuencia (H y V)
	COMPLEX *prodzh, *prodzv;	
	if (PINNED_MEMORY) {
		cudaHostAlloc((void**) &prodzh, resultados_size_complex, cudaHostAllocDefault);
		cudaHostAlloc((void**) &prodzv, resultados_size_complex, cudaHostAllocDefault);
	} else {
		prodzh = (COMPLEX*) malloc(resultados_size_complex);
		prodzv = (COMPLEX*) malloc(resultados_size_complex);
	}

	FLOAT *prodih, *prodiv, *prodRho;
	if (PINNED_MEMORY) {
		cudaHostAlloc((void**) &prodih, resultados_size_float, cudaHostAllocDefault);
		cudaHostAlloc((void**) &prodiv, resultados_size_float, cudaHostAllocDefault);
		cudaHostAlloc((void**) &prodRho, resultados_size_float, cudaHostAllocDefault);
	} else {
		prodih = (FLOAT*) malloc(resultados_size_float);
		prodiv = (FLOAT*) malloc(resultados_size_float);
		prodRho = (FLOAT*) malloc(resultados_size_float);
	}


	COMPLEX  *R_vh, *R_phi; //*R_a, *R_b;
	if (PINNED_MEMORY) {
	//	cudaHostAlloc((void**) &R_a, resultados_size_complex, cudaHostAllocDefault);
	//	cudaHostAlloc((void**) &R_b, resultados_size_complex, cudaHostAllocDefault);
		cudaHostAlloc((void**) &R_vh, resultados_size_complex, cudaHostAllocDefault);
		cudaHostAlloc((void**) &R_phi, resultados_size_complex, cudaHostAllocDefault);
	} else {
	//	R_a = (COMPLEX*) malloc(resultados_size_complex);
	//	R_b = (COMPLEX*) malloc(resultados_size_complex);
		R_vh = (COMPLEX*) malloc(resultados_size_complex);
		R_phi = (COMPLEX*) malloc(resultados_size_cuComplex);		
	}


	// delcaracion de las variables para la toma de tiempos	
	double t_total_transf, t_total_potencia, t_total_freq, t_total_autocorrelacion, t_total_coef_correlacion, t_RMA_total;
	t_total_freq = t_total_transf = t_total_potencia = t_total_autocorrelacion = t_total_coef_correlacion = t_RMA_total = 0.0;
	double t_total_corrimiento_fase = 0.0;


	if (!prodzh || !prodzv || !prodih || !prodiv ||  !R_vh || !R_phi) { 
		cout << "Alocacion de memoria en host falla " << endl; exit(EXIT_FAILURE); 
	}
	


/* punteros a funciones que dependen del codigo de optimiacion para los calculos*/
	p_funcion_potencia_type funcion_potencia;
	p_funcion_frecuencia_type funcion_frecuencia;
	p_funcion_autocorrelacion_type funcion_autocorrelacion;


	/* Depende del codigo de optimizacion llamo a distintas funciones secuenciales. Puntero a funciones. */
	switch (CODIGO_OPTIMIZACION) {
			case (0): funcion_potencia = calcular_potencia;
					  funcion_frecuencia = calcular_frecuencia;
					  funcion_autocorrelacion = calcular_autocorrelacion;
			          break;

			case (1): funcion_potencia = calcular_potencia_optimizacion_1;  // cambio patron de acceso a datos de entrada
			          funcion_frecuencia = calcular_frecuencia_optimizacion_1;	
			          funcion_autocorrelacion = calcular_autocorrelacion;
			          break;
			case (2): funcion_potencia = calcular_potencia_optimizacion_2;   // memoria compartida
			          funcion_frecuencia = calcular_frecuencia_optimizacion_2;
			          funcion_autocorrelacion = calcular_autocorrelacion;	
			          break;
			default: funcion_potencia = calcular_potencia; 
			         funcion_frecuencia = calcular_frecuencia;
			         funcion_autocorrelacion = calcular_autocorrelacion;
			         break;
	}



	/* SON COMPATIBLES complex de C con cuComplex porque vienen de C99 
	https://devtalk.nvidia.com/default/topic/486155/using-cucomplex-in-device-and-host-code/ */
	/* EN DEVICE VOY CON CUCUPLEX, EN CPU CON COMPLEX */


	/* PROCESO PRIMERO HORIZONTAL DESPUES VERTICAL, NO ENTRAN DATOS EN MEMORIA*/
	CUCOMPLEX *d_data_H, *d_data_V;
	cudaMalloc((void**)&d_data_H, matrix_size_cuComplex);
	cudaMalloc((void**)&d_data_V, matrix_size_cuComplex);
	
	/* resultados de POTENCIA en GPU */
	CUCOMPLEX *d_prodzh, *d_prodzv;
	cudaMalloc((void**)&d_prodzh, resultados_size_cuComplex);
	cudaMalloc((void**)&d_prodzv, resultados_size_cuComplex);

	FLOAT *d_prodi, *d_prodRho;
	cudaMalloc((void**)&d_prodi, resultados_size_float);
	cudaMalloc((void**)&d_prodRho, resultados_size_float);

	/* resultados de AUTOCORRELACION en GPU */
	CUCOMPLEX *d_R_vh, *d_R_phi;//, *d_R_a, *d_R_b, *d_prodzv;
//	cudaMalloc((void**)&d_R_a, resultados_size_cuComplex);
//	cudaMalloc((void**)&d_R_b, resultados_size_cuComplex);
	cudaMalloc((void**)&d_R_vh, resultados_size_cuComplex);
	cudaMalloc((void**)&d_R_phi, resultados_size_cuComplex);

	if(!d_data_H || !d_prodzh || !d_prodzv || !d_prodi || !d_data_V || !d_R_vh || !d_R_phi) {
		cout << "No aloca matrices en GPU " << endl; exit (EXIT_FAILURE); 
	}


	gpu_timer crono_gpu_transf;
	gpu_timer crono_gpu_potencia;
	gpu_timer crono_gpu_freq;
	gpu_timer crono_gpu_autocorrelacion;
	gpu_timer crono_gpu_coef_correlacion;
	gpu_timer crono_gpu_corrimiento_fase;
	cpu_timer crono_cpu_total;

	
	crono_cpu_total.tic();

	crono_gpu_transf.tic();
	/* copio los datos crudos a la GPU */
	if (cudaMemcpy(d_data_H, data_H, matrix_size, cudaMemcpyHostToDevice) != cudaSuccess) { cout << "Fallo en transferencia CPU -> GPU" << endl; exit(EXIT_FAILURE); };
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;

	
/* HORIZONTAL */
	
	crono_gpu_potencia.tic();
	/* CALCULO DE POTENCIA EN H */
	if (funcion_potencia(d_data_H, d_prodzh) != EXIT_SUCCESS) { cout << "Error calculo Potencia en H " << endl; exit(EXIT_FAILURE); };
	crono_gpu_potencia.tac();

	t_total_potencia += crono_gpu_potencia.ms_elapsed;


	crono_gpu_transf.tic();
	/* copio los datos crudos a la GPU */
	cudaMemcpy(prodzh, d_prodzh, resultados_size_cuComplex , cudaMemcpyDeviceToHost);
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;

	
	crono_gpu_freq.tic();
	/* CALCULO DE FRECUENCIA EN H */ 
	if (funcion_frecuencia(d_data_H, d_prodi) != EXIT_SUCCESS) { cout << "Error calculo frecuencia en H " << endl; exit(EXIT_FAILURE); };
	crono_gpu_freq.tac();

	t_total_freq += crono_gpu_freq.ms_elapsed;


	crono_gpu_transf.tic();
	/* copio los resultados desde GPU a CPU */
	cudaMemcpy(prodih, d_prodi, resultados_size_float , cudaMemcpyDeviceToHost);
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;


// no quiero incluir esta I/O en el tiempo total de computo
	crono_cpu_total.tac();
	t_RMA_total += crono_cpu_total.ms_elapsed;

	imprimir_matriz_float_a_archivo(prodih, 360, M, SALIDA_FRECUENCIA_H);


/* 	VERTICAL   */ 

	crono_cpu_total.tic();

	crono_gpu_transf.tic();
	/* PROCESO DATOS V; copio los datos crudos a la GPU */
	if (cudaMemcpy(d_data_V, data_V, matrix_size, cudaMemcpyHostToDevice) != cudaSuccess) {cout << "Error transferencia CPU -> GPU " << endl; exit(EXIT_FAILURE);};
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;
	

	crono_gpu_potencia.tic();
	/* CALCULO DE POTENCIA EN V */
	if (funcion_potencia(d_data_V, d_prodzv) != EXIT_SUCCESS) {cout <<  "Error calculo Potencia en V " << endl; exit(EXIT_FAILURE); };
	crono_gpu_potencia.tac();

	t_total_potencia += crono_gpu_potencia.ms_elapsed;


	crono_gpu_transf.tic();
	/* copio los datos crudos a la GPU */
	cudaMemcpy(prodzv, d_prodzv, resultados_size_complex , cudaMemcpyDeviceToHost);
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;


	crono_gpu_freq.tic();
	/* CALCULO DE FRECUENCIA EN V */ 
	if (funcion_frecuencia(d_data_V, d_prodi) != EXIT_SUCCESS) { cout << "Error calculo frecuencia en H " << endl; exit(EXIT_FAILURE); };
	crono_gpu_freq.tac();

	t_total_freq += crono_gpu_freq.ms_elapsed;


	crono_gpu_transf.tic();
	/* copio los resultados desde GPU a CPU */
	cudaMemcpy(prodiv, d_prodi, resultados_size_float , cudaMemcpyDeviceToHost);
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;


	// no quiero incluir esta I/O en el tiempo total de computo
	crono_cpu_total.tac();
	t_RMA_total += crono_cpu_total.ms_elapsed;

	
	cudaFree(d_prodi);
	imprimir_matriz_float_a_archivo(prodiv, 360, M, SALIDA_FRECUENCIA_V);


	crono_cpu_total.tic();

	//  AUTOCORRELACION R_b  
	int desdeCol1, hastaCol1, desdeCol2, hastaCol2;
/*	
	desdeCol1 = 0;
	hastaCol1 = PULSOS_GRUPO-1;
	desdeCol2 = 1;
	hastaCol2 = PULSOS_GRUPO;
	// calculo de la AUTOCORRELACION 
	crono_gpu_autocorrelacion.tic();
	if (funcion_autocorrelacion(d_data_H, desdeCol1, hastaCol1, d_data_V, desdeCol2, hastaCol2, d_R_a) != EXIT_SUCCESS) { cout << "Error calculo frecuencia en H " << endl; exit(EXIT_FAILURE); };
	crono_gpu_autocorrelacion.tac();

	t_total_autocorrelacion += crono_gpu_autocorrelacion.ms_elapsed;

*/
/*	crono_gpu_transf.tic();
	// copio los resultados desde GPU a CPU 
	cudaMemcpy(R_a, d_R_a, resultados_size_cuComplex , cudaMemcpyDeviceToHost);
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;
*/


	/*  AUTOCORRELACION R_b  */ 
/*	desdeCol1 = 0;
	hastaCol1 = PULSOS_GRUPO-1;
	desdeCol2 = 1;
	hastaCol2 = PULSOS_GRUPO;
	// calculo de la AUTOCORRELACION 
	crono_gpu_autocorrelacion.tic();
	if (funcion_autocorrelacion(d_data_V, desdeCol1, hastaCol1, d_data_H, desdeCol2, hastaCol2, d_R_b) != EXIT_SUCCESS) { cout << "Error calculo frecuencia en H " << endl; exit(EXIT_FAILURE); };
	crono_gpu_autocorrelacion.tac();

	t_total_autocorrelacion += crono_gpu_autocorrelacion.ms_elapsed;
*/


/*	crono_gpu_transf.tic();
	// copio los resultados desde GPU a CPU 
	cudaMemcpy(R_b, d_R_b, resultados_size_cuComplex , cudaMemcpyDeviceToHost);
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;
*/

	/*  AUTOCORRELACION R_vh */ 
	desdeCol1 = 0;
	hastaCol1 = PULSOS_GRUPO;
	desdeCol2 = 0;
	hastaCol2 = PULSOS_GRUPO;
	/* calculo de la AUTOCORRELACION */
	crono_gpu_autocorrelacion.tic();
	if (funcion_autocorrelacion(d_data_H, desdeCol1, hastaCol1, d_data_V, desdeCol2, hastaCol2, d_R_vh) != EXIT_SUCCESS) { cout << "Error calculo frecuencia en H " << endl; exit(EXIT_FAILURE); };
	crono_gpu_autocorrelacion.tac();

	t_total_autocorrelacion += crono_gpu_autocorrelacion.ms_elapsed;



	crono_gpu_transf.tic();
	/* copio los resultados desde GPU a CPU */
	cudaMemcpy(R_vh, d_R_vh, resultados_size_cuComplex , cudaMemcpyDeviceToHost);
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;



	/*  CORRIMIENTO DE FASE */ 
	crono_gpu_corrimiento_fase.tic();
	if (calcular_corrimiento_fase(d_R_vh, d_R_phi) != EXIT_SUCCESS) {
		cout << "Error calculo en corrimiento de fase " << endl; exit(EXIT_FAILURE); 
	}
	crono_gpu_corrimiento_fase.tac();

	t_total_corrimiento_fase += crono_gpu_corrimiento_fase.ms_elapsed;

	// Transferencia
	crono_gpu_transf.tic();
		/* copio los resultados desde GPU a CPU */
		cudaMemcpy(R_phi, d_R_phi, resultados_size_cuComplex, cudaMemcpyDeviceToHost);
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;


	// COEFICIENTE DE CORRELACION
	crono_gpu_coef_correlacion.tic();
	if (funcion_coef_correlacion(d_R_vh, d_prodzh, d_prodzv, d_prodRho) != EXIT_SUCCESS) { cout << "Error en calculo de coeficiente de correlacion " << endl; exit(EXIT_FAILURE); };
	crono_gpu_coef_correlacion.tac();
	t_total_coef_correlacion += crono_gpu_coef_correlacion.ms_elapsed;


	crono_gpu_transf.tic();
	/* copio los resultados desde GPU a CPU */
	cudaMemcpy(prodRho, d_prodRho, resultados_size_float , cudaMemcpyDeviceToHost);
	crono_gpu_transf.tac();

	t_total_transf += crono_gpu_transf.ms_elapsed;

	crono_cpu_total.tac();
	t_RMA_total += crono_cpu_total.ms_elapsed;

	/*  Se almacenan los resultados */
	imprimir_matriz_compleja_a_archivo_log(prodzh, 360, M, SALIDA_POTENCIA_H);
	imprimir_matriz_compleja_a_archivo_log(prodzv, 360, M, SALIDA_POTENCIA_V);

//	imprimir_matriz_compleja_a_archivo_log(R_a, 360, M, SALIDA_AUTOCORRELACION_A);
//	imprimir_matriz_compleja_a_archivo_log(R_b, 360, M, SALIDA_AUTOCORRELACION_B);
	imprimir_matriz_compleja_a_archivo_log(R_vh, 360, M, SALIDA_AUTOCORRELACION_VH);

	imprimir_matriz_float_a_archivo(prodRho, 360, M, SALIDA_COEF_CORRELACION);

	imprimir_matriz_compleja_a_archivo_log(R_phi, 360, M, SALIDA_CORRIMIENTO_DE_FASE);


	
	cout << "# Tiempo RMA Total " << t_RMA_total << " mseconds " << endl;
	cout << "# Tiempo de GPU Potencia " << t_total_potencia << " mseconds " << endl;
	cout << "# Tiempo de GPU Frecuencia " << t_total_freq << " mseconds " << endl;
	cout << "# Tiempo de GPU Autocorrelaciones " << t_total_autocorrelacion << " mseconds " << endl;
	cout << "# Tiempo de GPU coeficiente correlaciones " << t_total_coef_correlacion << " mseconds " << endl;
	cout << "# Tiempo de GPU Corrimiento de fase " << t_total_corrimiento_fase << " mseconds " << endl;
	cout << "# Tiempo de transferencia de datos  " << t_total_transf << " mseconds " << endl;

	
	


	if (PINNED_MEMORY) {
		cudaFreeHost(prodzh);
		cudaFreeHost(prodzv);
		cudaFreeHost(prodih);
		cudaFreeHost(prodiv);
//		cudaFreeHost(R_a);
//		cudaFreeHost(R_b);
		cudaFreeHost(R_vh);
		cudaFreeHost(prodRho);
		cudaFreeHost(R_phi);
	} else {
		free(prodzh);
		free(prodzv);
		free(prodih);
		free(prodiv);
//		free(R_a);
//		free(R_b);
		free(R_vh);
		free(prodRho);
		free(R_phi);
	}



		cudaFree(d_prodzh);
		cudaFree(d_prodzv);
	//	cudaFree(d_prodi);
		cudaFree(d_data_H);
		cudaFree(d_data_V);
//		cudaFree(d_R_a);
//		cudaFree(d_R_b);
		cudaFree(d_R_vh);
		cudaFree(d_prodRho);
		cudaFree(d_R_phi);

	

	return (EXIT_SUCCESS);
}



int calcular_potencia(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz ) {

	// instancio 1 thread por dato en el resultado. Los resultados son matrices de M x 360 
	dim3 size_block(NTHREADS_X, NTHREADS_Y);
	dim3 size_grid( (M + (size_block.x - 1)) / size_block.x , ((FILAS_SALIDA + (size_block.y - 1)) / size_block.y));

	/* calculo  */
	// kernel_calcular_potencia<<<size_grid, size_block>>>(d_data, d_prodz);
	kernel_nueva_potencia<<<size_grid, size_block>>>(d_data, d_prodz);
	cudaDeviceSynchronize();

	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess) { cout << "Error ejecutando el kernel potencia:  " << cudaGetErrorString(err) <<  endl; return (EXIT_FAILURE);};


	return EXIT_SUCCESS;
}

__global__ void kernel_nueva_potencia(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz) {
	// Offsets para la matriz de salida (d_prodz)
	int fila_salida = threadIdx.y + (blockIdx.y * blockDim.y);
	int col_salida	= threadIdx.x + (blockIdx.x * blockDim.x);
	int grupo = fila_salida;

	// Offsets para la matriz de entrada (d_data)
	int fila_entrada = col_salida * N;
	int col_entrada	= PRIMER_PULSO_BARRIDO + (PULSOS_GRUPO * grupo);

	if ((fila_salida < FILAS_SALIDA) && (col_salida < M)) {
		CUCOMPLEX sum = {0.0, 0.0};
		CUCOMPLEX aux = {0.0, 0.0};

		for (int k=0; k < PULSOS_GRUPO; k++) {
			aux = producto_cuFloatComplex(
				d_data[fila_entrada + col_entrada + k], 
				conjugado_cuFloatComplex(d_data[fila_entrada + col_entrada + k])
			);

			sum = suma_cuFloatComplex(sum, aux);
			
		}

		// Guardamos el elemento de la diagonal en la fila de salida
		d_prodz[(fila_salida * M) + col_salida] = sum;
	}
}


/* IMPLEMENTACION DE KERNELS PARA POTENCIA */
__global__ void kernel_calcular_potencia(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz) {


	//  fila del thread
	int fila_salida =  ((blockDim.y * blockIdx.y) + (threadIdx.y)) ; 
	// columna de la entrada
	int col_salida =  threadIdx.x + (blockDim.x * blockIdx.x);

	// nro_grupo = son 360 grupos y es la fila del thread =  la fila en la salida
	int nro_grupo = fila_salida ; 

	// la entrada es de 2242 filas, y esto es la columna del thread 
	int fila_datos_entrada = col_salida;
	// es la posicion 0 del grupo
	int col_datos_entrada = nro_grupo * PULSOS_GRUPO;


	CUCOMPLEX aux = {0.0, 0.0};
	CUCOMPLEX resu_producto;
	
	int k;
 	/*CALCULAR POTENCIA Poh=diag(d1h*d1h') */
	if ((fila_salida < FILAS_SALIDA) && (col_salida < M)){
		for (k = 0; k < PULSOS_GRUPO; k++) {

			resu_producto = producto_cuFloatComplex(
				d_data[(fila_datos_entrada * N) + (PRIMER_PULSO_BARRIDO + col_datos_entrada + k)], 
				conjugado_cuFloatComplex(d_data[(fila_datos_entrada * N) + (PRIMER_PULSO_BARRIDO + col_datos_entrada + k)])
			);
    		aux = suma_cuFloatComplex(aux, resu_producto);
    
		}
		d_prodz[(fila_salida*M) + col_salida] = aux;

	}
}




int calcular_frecuencia(CUCOMPLEX *d_data, FLOAT *d_prodi ) {

	// instancio 1 thread por dato en el resultado. Los resultados son matrices de M x 360 
	dim3 size_block(NTHREADS_X, NTHREADS_Y);
	dim3 size_grid( (M + (size_block.x - 1)) / size_block.x , ((FILAS_SALIDA + (size_block.y - 1)) / size_block.y));

	/* calculo  */
	kernel_calcular_frecuencia<<<size_grid, size_block>>>(d_data, d_prodi);
	cudaDeviceSynchronize();

	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess) { cout << "Error ejecutando el kernel frecuencia:  " << cudaGetErrorString(err) <<  endl; return (EXIT_FAILURE);};


	return EXIT_SUCCESS;
}




/* IMPLEMENTACION DE KERNELS PARA POTENCIA */
__global__ void kernel_calcular_frecuencia(CUCOMPLEX *d_data, FLOAT *d_prodi) {


	//  fila del thread
	int fila_salida =  ((blockDim.y * blockIdx.y) + (threadIdx.y)) ; 
	// columna de la entrada
	int col_salida =  threadIdx.x + (blockDim.x * blockIdx.x);

	// nro_grupo = son 360 grupos y es la fila del thread =  la fila en la salida
	int nro_grupo = fila_salida ; 

	// la entrada es de 2242 filas, y esto es la columna del thread 
	int fila_datos_entrada = col_salida;
	// es la posicion 0 del grupo
	int col_datos_entrada = nro_grupo * PULSOS_GRUPO;


	CUCOMPLEX aux = {0.0, 0.0};
	CUCOMPLEX resu_producto;
	
	int k;
 	/*CALCULAR FRECUENCIA 
			T=1;
            sd1h=diag(d1h(:,1:end-1)*d1h(:,2:end)');
            foh=-angle(sd1h)/2/pi/T;

 	 */
	if ((fila_salida < FILAS_SALIDA) && (col_salida < M)){
		for (k = 0; k < PULSOS_GRUPO-1; k++) {

			resu_producto = producto_cuFloatComplex(
					            d_data[(fila_datos_entrada * N) + (PRIMER_PULSO_BARRIDO + col_datos_entrada + k)], 
					            conjugado_cuFloatComplex(d_data[(fila_datos_entrada * N) + (PRIMER_PULSO_BARRIDO + col_datos_entrada + (k+1))])
				    		);
			
			aux = suma_cuFloatComplex(aux, resu_producto);
		
		}
		
		d_prodi[(fila_salida*M) + col_salida] = ((((-1.0) * kernel_angle(aux)) * 0.5f) / CUDART_PI_F) / T;

	}
}


/* CALCULO DE LA AUTOCORRELACION */

int calcular_autocorrelacion(CUCOMPLEX *d_data1, int desdeCol1, int hastaCol1,  CUCOMPLEX *d_data2, int desdeCol2, int hastaCol2, CUCOMPLEX *d_R ) {

	// instancio 1 thread por dato en el resultado. Los resultados son matrices de M x 360 
	dim3 size_block(NTHREADS_X, NTHREADS_Y);
	dim3 size_grid( (M + (size_block.x - 1)) / size_block.x , ((FILAS_SALIDA + (size_block.y - 1)) / size_block.y));

	/* calculo  */
	kernel_calcular_autocorrelacion<<<size_grid, size_block>>>(d_data1, desdeCol1, hastaCol1, d_data2, desdeCol2, hastaCol2, d_R);
	cudaDeviceSynchronize();

	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess) { cout << "Error ejecutando el kernel Autocorrelacion con R_a:  " << cudaGetErrorString(err) <<  endl; return (EXIT_FAILURE);};

	
	return EXIT_SUCCESS;
}


__global__ void	kernel_calcular_autocorrelacion(CUCOMPLEX *d_data1, int desdeCol1, int hastaCol1,CUCOMPLEX *d_data2, int desdeCol2, int hastaCol2, CUCOMPLEX *d_R) {


	//  fila del thread
	int fila_salida =  ((blockDim.y * blockIdx.y) + (threadIdx.y)) ; 
	// columna de la entrada
	int col_salida =  threadIdx.x + (blockDim.x * blockIdx.x);

	// nro_grupo = son 360 grupos y es la fila del thread =  la fila en la salida
	int nro_grupo = fila_salida ; 

	// la entrada es de 2242 filas, y esto es la columna del thread 
	int fila_datos_entrada = col_salida;
	// es la posicion 0 del grupo
	int col_datos_entrada = nro_grupo * PULSOS_GRUPO;


	CUCOMPLEX aux = {0.0, 0.0};
	CUCOMPLEX resu_producto;
	int diferencia_columnas = hastaCol1 - desdeCol1;
	
	int k;
 	/*     */ 
	if ((fila_salida < FILAS_SALIDA) && (col_salida < M)){
		for (k = 0; k < diferencia_columnas; k++) {

			resu_producto = producto_cuFloatComplex(
					            d_data1[(fila_datos_entrada * N) + (PRIMER_PULSO_BARRIDO + col_datos_entrada + (k+desdeCol1))], 
					            conjugado_cuFloatComplex(d_data2[(fila_datos_entrada * N) + (PRIMER_PULSO_BARRIDO + col_datos_entrada + (k+desdeCol2))])
				    		);
			
			aux = suma_cuFloatComplex(aux, resu_producto);
		
		}
		
		d_R[(fila_salida*M) + col_salida] =  division_cuComplex(aux, PULSOS_GRUPO) ;
		

	}


}



	int funcion_coef_correlacion(CUCOMPLEX *d_R_vh, CUCOMPLEX *d_prodzh, CUCOMPLEX *d_prodzv, FLOAT *d_prodRho) {

		// instancio 1 thread por dato en el resultado. Los resultados son matrices de M x 360 
		dim3 size_block(NTHREADS_X, NTHREADS_Y);
		dim3 size_grid( (M + (size_block.x - 1)) / size_block.x , ((FILAS_SALIDA + (size_block.y - 1)) / size_block.y));

		/* calculo  */
		kernel_calcular_coef_correlacion<<<size_grid, size_block>>>(d_R_vh, d_prodzh, d_prodzv, d_prodRho);
		cudaDeviceSynchronize();

		cudaError_t err = cudaGetLastError();
		if (err != cudaSuccess) { cout << "Error ejecutando el kernel coeficiente de autocorrelacion:  " << cudaGetErrorString(err) <<  endl; return (EXIT_FAILURE);};

		
		return EXIT_SUCCESS;
	}


__global__ void	kernel_calcular_coef_correlacion(CUCOMPLEX *d_R_vh, CUCOMPLEX *d_prodzh, CUCOMPLEX *d_prodzv, FLOAT *d_prodRho) {

	// rho_0=R_vh./sqrt(Pov.*Poh);
	// prodRho(i,:)=abs(rho_0);

	int fila_salida =  (blockDim.y * blockIdx.y) + threadIdx.y ; 
	int col_salida =  threadIdx.x + (blockDim.x * blockIdx.x);
	int indice = fila_salida * M + col_salida;

	// voy a trabajar todo con la parte real, ya que en matlab prodza y prodzv son real y no complex y 
	// graficando la parte real es igual matlab y CUDA

	if ((fila_salida < FILAS_SALIDA) && (col_salida < M))
	{
		FLOAT aux;
		aux = sqrt(d_prodzv[indice].x * d_prodzh[indice].x);
	
		d_prodRho[indice] = abs_cuComplex(division_cuComplex(d_R_vh[indice], aux));;
	}

}







int calcular_corrimiento_fase(CUCOMPLEX *d_R_vh, CUCOMPLEX *d_R_phi) {

	// instancio 1 thread por dato en el resultado. Los resultados son matrices de M x 360 
	dim3 size_block(NTHREADS_X, NTHREADS_Y);
	dim3 size_grid( (M + (size_block.x - 1)) / size_block.x , ((FILAS_SALIDA + (size_block.y - 1)) / size_block.y));

	/* calculo  */
	kernel_calcular_corrimiento_fase<<<size_grid, size_block>>>(d_R_vh, d_R_phi);
	cudaDeviceSynchronize();

	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess) {
		cout << "Error ejecutando el kernel Corrimiento de fase:  " << cudaGetErrorString(err) <<  endl; return (EXIT_FAILURE);
	}
	
	return EXIT_SUCCESS;
}


__global__ void	kernel_calcular_corrimiento_fase(CUCOMPLEX *d_R_vh, CUCOMPLEX *d_R_phi) {
	//  fila del thread
	int fila_salida =  ((blockDim.y * blockIdx.y) + (threadIdx.y)) ; 
	// columna de la entrada
	int col_salida =  threadIdx.x + (blockDim.x * blockIdx.x);

	CUCOMPLEX aux = {0.0, 0.0};

	if ((fila_salida < FILAS_SALIDA) && (col_salida < M)) {
		
		// Temporal, intento de obtener resultados
		aux.x = kernel_angle(d_R_vh[(fila_salida*M) + col_salida]);
		aux.y = 0.0;
		d_R_phi[(fila_salida*M) + col_salida] = aux;
		
	}

}

/****************************************************************************************/
/*                                OPTIMIZACION 1                                        */
/****************************************************************************************/


int calcular_potencia_optimizacion_1(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz) {

    // instancio 1 thread por dato en el resultado. Los resultados son matrices de M x 360 
	dim3 size_block(64,8);
	dim3 size_grid( (360 + (size_block.x - 1)) / size_block.x , ((2242 + (size_block.y - 1)) / size_block.y));

	/* calculo  */
	kernel_calcular_potencia_optimizacion_1<<<size_grid, size_block>>>(d_data, d_prodz);
	cudaDeviceSynchronize();

	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess) { cout << "Error ejecutando el kernel potencia optimizacion 1: " << cudaGetErrorString(err) <<  endl; return (EXIT_FAILURE);};


	return EXIT_SUCCESS;
}



/* IMPLEMENTACION DE KERNELS PARA POTENCIA */
__global__ void kernel_calcular_potencia_optimizacion_1(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz) {

	//  fila del thread
	int mi_fila =  ((blockDim.y * blockIdx.y) + (threadIdx.y)) ; 
	// columna de la entrada
	int mi_col =  threadIdx.x + (blockDim.x * blockIdx.x);

	//  fila del thread
	int fila_salida =  mi_col ; 
	// columna de la entrada
	int col_salida =  mi_fila;

	// nro_grupo = son 360 grupos y es la fila del thread =  la fila en la salida
//	int nro_grupo = mi_col; 

	// la entrada es de 2242 filas, y esto es la columna del thread 
	int fila_datos_entrada = mi_fila;
	// es la posicion 0 del grupo
	int col_datos_entrada = mi_col * PULSOS_GRUPO;//nro_grupo * PULSOS_GRUPO;


	CUCOMPLEX aux = {0.0, 0.0};
	CUCOMPLEX dato_matriz = {0.0, 0.0};
	CUCOMPLEX resu_producto;
	
	int k;
 	
	if ((mi_fila < 2242) && (mi_col < 360)){

	//	if (threadIdx.x < PULSOS_GRUPO) {
			for (k = 0; k < PULSOS_GRUPO; k++) {

				dato_matriz = d_data[(fila_datos_entrada * N) + (PRIMER_PULSO_BARRIDO + col_datos_entrada + k)];
				resu_producto = producto_cuFloatComplex(dato_matriz, conjugado_cuFloatComplex(dato_matriz));
    			aux = suma_cuFloatComplex(aux, resu_producto);
    		}
    
	//	}
		d_prodz[(fila_salida*M) + col_salida] = aux;

	}
}




int calcular_frecuencia_optimizacion_1(CUCOMPLEX *d_data, FLOAT *d_prodi) {



	return 0;
}



/****************************************************************************************/
/*                                OPTIMIZACION 2                                        */
/****************************************************************************************/


int calcular_potencia_optimizacion_2(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz) {

     // instancio 1 thread por dato en el resultado. Los resultados son matrices de M x 360 
	dim3 size_block(64,8);
	dim3 size_grid( (360 + (size_block.x - 1)) / size_block.x , ((2242 + (size_block.y - 1)) / size_block.y));

	/* calculo  */
	kernel_calcular_potencia_optimizacion_2<<<size_grid, size_block>>>(d_data, d_prodz);
	cudaDeviceSynchronize();

	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess) { cout << "Error ejecutando el kernel potencia optimizacion 1: " << cudaGetErrorString(err) <<  endl; return (EXIT_FAILURE);};


	return EXIT_SUCCESS;
}



/* IMPLEMENTACION DE KERNELS PARA POTENCIA */
__global__ void kernel_calcular_potencia_optimizacion_2(CUCOMPLEX *d_data, CUCOMPLEX *d_prodz) {

// NO ANDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA 

	//  fila del thread
	int mi_fila =  ((blockDim.y * blockIdx.y) + (threadIdx.y)) ; 
	// columna de la entrada
	int mi_col =  threadIdx.x + (blockDim.x * blockIdx.x);

	//  fila del thread
	int fila_salida =  mi_col ; 
	// columna de la entrada
	int col_salida =  mi_fila;

	// nro_grupo = son 360 grupos y es la fila del thread =  la fila en la salida
	int nro_grupo = mi_col; 

	// la entrada es de 2242 filas, y esto es la columna del thread 
	int fila_datos_entrada = mi_fila;
	// es la posicion 0 del grupo
	int col_datos_entrada = nro_grupo * PULSOS_GRUPO;


	CUCOMPLEX aux = {0.0, 0.0};
	CUCOMPLEX resu_producto;
	
	int k;
 	/*CALCULAR POTENCIA Poh=diag(d1h*d1h') */
	if ((mi_fila < 2242) && (mi_col < 360)){


		__shared__ CUCOMPLEX datos_sh[PULSOS_GRUPO*8];

	//	datos_sh[0] = nro_grupo * PULSOS_GRUPO ;
		
        for (k = 0; k < PULSOS_GRUPO; k++) 
        	datos_sh[k] = d_data[(fila_datos_entrada * N) + (PRIMER_PULSO_BARRIDO + col_datos_entrada + k)];
		__syncthreads();

	//	if (threadIdx.x < PULSOS_GRUPO) {
			
		for (k = 0; k < PULSOS_GRUPO; k++) {

	
/*			resu_producto = producto_cuFloatComplex_segundo_argumento_conjugado(d_data[(fila_datos_entrada * N) + (PRIMER_PULSO_BARRIDO + col_datos_entrada + k)], d_data[(fila_datos_entrada * N) + (PRIMER_PULSO_BARRIDO + col_datos_entrada + k)]);
*/
			resu_producto = producto_cuFloatComplex(datos_sh[k], conjugado_cuFloatComplex(datos_sh[k]));


    		aux = suma_cuFloatComplex(aux, resu_producto);
    	}
    
	//	}
		d_prodz[(fila_salida*M) + col_salida] = aux;

	}
}




int calcular_frecuencia_optimizacion_2(CUCOMPLEX *d_data, FLOAT *d_prodi) {



	return 0;
}





/****************************************************************************************/
/*                                FUNCIONES AUXILIARES                                  */
/****************************************************************************************/




/* funciones device para operar con cuFloatComplex*/

__device__ CUCOMPLEX producto_cuFloatComplex(CUCOMPLEX a, CUCOMPLEX b) {

	CUCOMPLEX aux;

	aux.x = (a.x * b.x) - (a.y * b.y);
	aux.y = (a.x * b.y) + (a.y * b.x);


	return aux;
}

__device__ CUCOMPLEX conjugado_cuFloatComplex(CUCOMPLEX a) {
  // ToDo: bitwise op. podría ser más rápida
	a.y = a.y * -1.0;
	return a;
}

__device__ CUCOMPLEX suma_cuFloatComplex(CUCOMPLEX a, CUCOMPLEX b) {

	CUCOMPLEX aux;

	aux.x = (a.x + b.x);
	aux.y = (a.y + b.y);


	return aux;
}


__device__ CUCOMPLEX division_cuComplex(CUCOMPLEX a, FLOAT b) {

	CUCOMPLEX aux;

	aux.x = ((FLOAT)a.x / (FLOAT) b);
	aux.y = ((FLOAT)a.y / (FLOAT) b);

	return aux;
}

__device__ FLOAT kernel_angle(CUCOMPLEX valor) {

    FLOAT aux;

    // por defincion atan2(parte_imaginaria, parte_real)
    aux = atan2(valor.y, valor.x);
    return aux;

}


__device__ FLOAT abs_cuComplex(CUCOMPLEX num){

	return sqrt(pow(num.x, 2) + pow(num.y,2));

}
